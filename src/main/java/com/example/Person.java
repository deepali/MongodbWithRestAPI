package com.example;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

/**
 * Created by Softhard-DLT1 on 2/21/2017.
 */

@Document
public class Person {

    //Specifies the primary key of an entity and automatically generated
    @Id
    private String Id;

    private String firstName;
    private String lastName;

    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName=firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName=lastName;
    }
}
