package com.example;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by Softhard-DLT1 on 2/21/2017.
 */

//Create a Person repository

@Transactional
@Repository
public interface PersonRepository extends MongoRepository{

   public List<Person> findAll();
}
