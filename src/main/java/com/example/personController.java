package com.example;

/**
 * Created by Softhard-DLT1 on 2/22/2017.
 */
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.PersonRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/Persons")
public class personController {

    @Autowired
    PersonRepository personRepository;

    @RequestMapping(value="/show",method = RequestMethod.GET)
    public Map<String, Object> readAll() {
        List<Person> personList = personRepository.findAll();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("message", "Booking found successfully");
        dataMap.put("totalBooking", personList.size());
        dataMap.put("status", "1");
        dataMap.put("bookings", personList);
        return dataMap;
    }
}
